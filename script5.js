/*Teste 5 números inteiros aleatórios. Os testes:
● Caso o valor seja divisível por 3, imprima no console “fizz”.
● Caso o valor seja divisível por 5 imprima “buzz”.
● Caso o valor seja divisível por 3 e 5, ao mesmo tempo, imprima
“fizzbuzz”.
● Caso contrário imprima nada.
*/
for (var c = 0; c <= 5; c++){
    var int = prompt("Digite um número: ")
    if (int % 3 == 0 && int % 5 != 0){
        console.log("fizz")
    }
    if (int % 5 == 0 && int % 3 != 0){
        console.log("buzz")
    }
    if (int % 5 == 0 && int % 3 == 0){
        console.log("fizzbuzz")
    }
    else {
        console.log("nada")
    }
}